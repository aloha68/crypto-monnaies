#!/usr/bin/env python3

import logging
import sys

# Cet import permet de lancer l'orm django
import manage

logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)


if __name__ == '__main__':

    from console import ConsoleInteractive
    ConsoleInteractive().start()
    sys.exit()