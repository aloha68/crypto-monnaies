
from . import ConsoleModule
from .utils import format_value, ConsoleStyleMixin

import logging
logger = logging.getLogger(__name__)


class ConsoleBuyer(ConsoleStyleMixin, ConsoleModule):
    """
    Module permettant de simuler un achat, c'est-à-dire
    d'estimer la valeur d'une monnaie à un moment précis.
    """
    commande = 'buy'
    titre = "Simuler un achat"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.monnaie = ""
        self.nombre = 1
        self.usd_value = 0

    def fill_arguments(self, parser):
        """Remplit les arguments saisissables en ligne de commande"""

        parser.add_argument(
            'nombre', type=float, nargs='?', default=1,
            help="Nombre de jetons à acheter (par défaut : 1)"
        )
        parser.add_argument(
            'monnaie', type=str,
            help="Symbole représentant la monnaie (en trois lettres)"
        )
        parser.add_argument(
            '--usd', type=float,
            help="Nombre de dollars à investir"
        )

    def parse_arguments(self, **kwargs):
        """Récupère les arguments passés à la ligne de commande"""

        monnaie = kwargs.get('monnaie', None)
        if not monnaie:
            raise ValueError("Il est nécessaire de renseigner la monnaie à acheter !")

        if len(monnaie) != 3:
            raise ValueError("Le paramètre 'monnaie' doit être un symbole de trois lettres !")

        self.monnaie = monnaie.lower()
        self.nombre = kwargs.get('nombre', 1)

        usd = kwargs.get('usd', 0)
        if usd:
            self.usd_value = usd

    def print_pair(self, pair):
        """Affiche les détails d'une paire"""

        monnaie = pair[3:]
        usd_pair = monnaie + 'usd'

        ticker = self.api.get_tick(pair)
        usd_ticker = self.api.get_tick(usd_pair)

        iota_usd_via_monnaie = self.nombre * ticker.last_price * usd_ticker.last_price

        print("  {} {} = {} {} si paiement en {}".format(
            self.nombre,
            ticker.currency_from.upper(),
            format_value(iota_usd_via_monnaie),
            usd_ticker.currency_to.upper(),
            ticker.currency_to.upper()))

    def buy_usd(self):
        """Affiche le nombre possible d'achat selon la valeur USD renseignée"""

        usd_pair = self.monnaie + 'usd'
        tick = self.api.get_tick(usd_pair)

        value = self.usd_value / tick.last_price
        if value > 1:
            value = round(value, 3)

        print("  Vous pouvez acheter {} {} pour {} {}".format(
            format_value(value),
            tick.currency_from.upper(),
            self.usd_value,
            tick.currency_to.upper()
        ))

    def start(self):
        """Démarre le module"""

        print("")
        if self.usd_value:
            self.buy_usd()
            print("")
            return

        logger.debug("Tentative d'achat de {} {}".format(self.nombre, self.monnaie))

        # On récupère toutes les pairs qui commencent par le symbole de la monnaie
        pairs = [sym for sym in self.api.pairs if sym.startswith(self.monnaie)]

        # On récupère déjà la valeur en dollar si elle existe
        usd_pair = self.monnaie + 'usd'
        if usd_pair in pairs:
            pairs.remove(usd_pair)

            tick = self.api.get_tick(usd_pair)
            value = tick.last_price * self.nombre

            print("{:%H:%M:%S}:".format(tick.date))
            print("  {} {} = {} {}".format(
                self.nombre, tick.currency_from.upper(),
                format_value(value), tick.currency_to.upper()))

        for pair in pairs:
            self.print_pair(pair)

        print("")