import logging

from .utils import ConsoleModule, ConsoleStyleMixin

logger = logging.getLogger(__name__)


class ConsoleConverter(ConsoleStyleMixin, ConsoleModule):
    """
    Module permettant de convertir la valeur d'une monnaie dans une autre.
    """
    commande = 'convert'
    titre = "Convertir une monnaie dans une autre"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.monnaie_in = ""
        self.monnaie_out = ""
        self.valeur = 0

    def fill_arguments(self, parser):
        """Remplit les arguments saisissables en ligne de commande"""

        parser.add_argument(
            '-in', type=str, required=True,
            help="Monnaie entrante"
        )
        parser.add_argument(
            '-out', type=str,
            help="Monnaie sortante"
        )
        parser.add_argument(
            'valeur', type=float,
            help="Valeur de la monnaie à convertir"
        )

    def parse_arguments(self, **kwargs):
        """Récupère les arguments passés à la ligne de commande"""

        self.monnaie_in = kwargs.get('in', None)
        if len(self.monnaie_in) != 3:
            raise ValueError("Le paramètre 'in' doit être un symbole de trois lettres !")

        self.monnaie_out = kwargs.get('out', None)
        if self.monnaie_out and len(self.monnaie_out) != 3:
            raise ValueError("Le paramètre 'out' doit être un symbole de trois lettres !")

        self.valeur = kwargs.get('valeur', None)

    def print_pair(self, pair):
        """Affiche une paire"""

        if pair not in self.api.pairs:
            print("  Cette paire n'est pas référencée dans l'API bitfinex : " + pair)
            return

        tick = self.api.get_tick(pair)
        result = self.valeur * tick.last_price

        str_data = "  {} {} = {} {}".format(
            self.valeur,
            tick.currency_from.upper(),
            self.format_value(result, self.Formats.BOLD),
            tick.currency_to.upper()
        )

        if self.valeur != 1:
            str_data += " (prix unitaire : {})".format(self.format_value(tick.last_price))

        print(str_data)

    def start(self):
        """Démarre le module"""

        print("")
        logger.debug("Module de convertion :")

        if self.monnaie_out:
            logger.debug("Convertir {} {} en {}".format(
                self.valeur, self.monnaie_in, self.monnaie_out
            ))

            pair = self.monnaie_in + self.monnaie_out
            self.print_pair(pair)
            return

        for pair in self.api.pairs:
            if pair.startswith(self.monnaie_in):
                self.print_pair(pair)

        print("")
