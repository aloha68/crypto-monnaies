
import logging

from core.workers import TickerWatcher
from .utils import ConsoleModule, ConsoleColor, format_value

logger = logging.getLogger(__name__)


class ConsoleWatcher(ConsoleModule):
    """
    Module permettant de suivre des indices périodiquement.
    À chaque interrogation de l'API bitfinex, une nouvelle
    ligne est ajoutée à la console pour indiquer le taux en
    cours pour la liste des paires à observer.
    """
    commande = 'watch'
    titre = "Observer le marché"

    # ROW FORMAT "_=_BTC-IOT_  LAST PRICE_VALUE       _LOW         _HIGH        _%H:%M:%S_DIFF
    ROW_HEADER = "    DEVISE DERNIER PRIX MOYENNE      MINIMUM      MAXIMUM      DATE     DIFF"
    ROW_FORMAT = " {status} {currency}-{destination} {last_price:<12} {mid:<12} {bid:<12} {ask:<12} {date:%H:%M:%S} {diff}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.max_errors = 0
        self.time_between_requests = 30
        self.watchers = []

    def fill_arguments(self, parser):
        """Remplit les arguments saisissables en ligne de commande"""
        parser.add_argument(
            'pairs', metavar='pair', type=str, nargs='+',
            help="Pair à suivre (ex: btcusd, iotusd, iotbtc)"
        )

    def parse_arguments(self, **kwargs):
        """Récupère les arguments passés à la ligne de commande"""

        # Définition du temps entre chaque requête
        #time_between_requests = self.args.time_between_request
        #if not time_between_requests:
        #    time_between_requests = round(60 / MAX_REQUESTS_PER_MINUTES * nb_watch)
        #logger.debug("Time between each request: {}".format(time_between_requests))

        pairs = kwargs.get('pairs')
        for pair in pairs:
            if pair.endswith(','):
                pair = pair[:-1]
            pair = pair.lower()
            self.add_watcher(pair)

    def _get_status(self, watcher):
        """Return status of this transaction: could be +, - or ="""

        status = "="
        current = watcher.current_ticker
        previous = watcher.last_ticker

        # If there is a previous ticker
        if previous:
            if current.last_price > previous.last_price:
                status = ConsoleColor.GREEN + "+"
            elif current.last_price < previous.last_price:
                status = ConsoleColor.RED + "-"

        return ConsoleColor.BOLD + status + ConsoleColor.END

    def _get_diff(self, watcher):
        """Retourne le pourcentage de différence avec la valeur du lancement"""

        first = watcher.first_ticker
        current = watcher.current_ticker
        diff = round((current.last_price - first.last_price) / first.last_price * 100, 2)

        color = ""
        if diff > 0:
            color = ConsoleColor.GREEN + "+"
        elif diff < 0:
            color = ConsoleColor.RED

        result = color + str(diff) + "%"
        return ConsoleColor.BOLD + result + ConsoleColor.END

    def _get_value_from_watcher(self, watcher, attr):
        """Get the attr attribute from watcher"""
        value = getattr(watcher.current_ticker, attr)
        return value

    def show_watcher(self, watcher):
        """Show current ticker from watcher"""

        ticker = watcher.current_ticker

        # Calculate status and give it style
        status = self._get_status(watcher)

        # Add bold around currencies
        currency = ConsoleColor.BOLD + watcher.currency
        destination = watcher.destination + ConsoleColor.END

        # Add diff
        diff = self._get_diff(watcher)

        print(ConsoleWatcher.ROW_FORMAT.format(
            status=status,
            currency=currency,
            destination=destination,
            last_price=format_value(ticker.last_price),
            mid=format_value(ticker.mid),
            bid=format_value(ticker.bid),
            ask=format_value(ticker.ask),
            date=ticker.date,
            diff=diff))

    def add_watcher(self, pair):
        """Add a watcher for a specific pair"""

        if pair not in self.api.pairs:
            logger.error("add_watcher({}): Symbol not found".format(pair))
            return

        if pair in self.watchers:
            logger.warning("add_watcher({}): Symbol already exists".format(pair))
            return

        watcher = TickerWatcher(self.api, pair,
                                callback=self.show_watcher,
                                max_errors=self.max_errors,
                                waiting_time=self.time_between_requests)
        self.watchers.append(watcher)

        logger.debug("ConsoleWatcher: add_watcher: {}".format(pair))

    def start(self):
        """Start all watchers"""

        if not self.watchers:
            logger.warning("Aucun observateur à suivre")
            return

        logger.debug("start()")
        logger.debug("{} observateurs trouvés: {}".format(
            len(self.watchers), ", ".join([w.symbol for w in self.watchers])
        ))

        for watcher in self.watchers:
            watcher.start()

        print("")
        print(ConsoleWatcher.ROW_HEADER)

        try:
            for watcher in self.watchers:
                watcher.join()
        except KeyboardInterrupt:
            for watcher in self.watchers:
                watcher.stop()
                watcher.join()
