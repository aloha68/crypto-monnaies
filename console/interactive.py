import argparse
import logging
import sys

from core.factories import APIFactory
from .utils import ConsoleModule

logger = logging.getLogger(__name__)

#
# Pour générer un bloc d'aide global lors de la commande -h
# https://stackoverflow.com/questions/20094215/argparse-subparser-monolithic-help-output
#


class ConsoleInteractive:

    def __init__(self):
        api = APIFactory.get_api()

        self._args = []
        self.modules = [m(api) for m in ConsoleModule.__subclasses__()]

        if not self.modules:
            raise RuntimeError("Aucun module trouvé !")

    @property
    def args(self):
        if self._args:
            return self._args

        self._args = self.get_arguments()
        return self._args

    def get_parser(self):
        """Récupère les arguments directement rattachés au programme"""

        parser = argparse.ArgumentParser(
            prog="crypto.py",
            description="Parseur de crypto-monnaie homemade :-)"
        )
        parser.add_argument(
            '-v',
            help="Exécute le programme en mode débug",
            action='count', dest='verbose', default=0
        )

        sub_parser = parser.add_subparsers()

        for m in self.modules:
            module_parser = sub_parser.add_parser(m.commande, help=m.titre)
            m.fill_arguments(module_parser)
            module_parser.set_defaults(mod=m)

        return parser

    def get_arguments(self):
        """Récupère les arguments directement rattachés au programme"""
        parser = self.get_parser()
        return parser.parse_args()

    def parse_arguments(self):
        """Parse les arguments directement rattachés au programme"""

        log_level = logging.WARNING
        if self.args.verbose == 1:
            log_level = logging.INFO
        elif self.args.verbose > 1:
            log_level = logging.DEBUG

        logging.basicConfig(level=log_level)

    def load_module(self):
        """Démarre un module à partir des arguments du programme"""

        if not isinstance(self.args.mod, ConsoleModule):
            raise ValueError("Le module doit implémenté la classe ConsoleModule")

        mod = self.args.mod
        logger.debug("Chargement du module {}".format(mod.commande))

        try:
            mod.parse_arguments(**vars(self.args))
        except ValueError as e:
            print(str(e))
            return

        mod.start()

    def start(self):

        logger.debug("Lancement du programme principal !")
        self.parse_arguments()

        # S'il y a un argument "mod", et donc qu'on veut lancer directement un module
        if hasattr(self.args, 'mod'):
            self.load_module()
            return

        logger.debug("Modules trouvés: {}".format(
            ', '.join([m.__class__.__name__ for m in self.modules])))

        print("")
        print("Bienvenue dans la console interactive :-)")
        print("")
        print("Euh... En fait non, à++ !")
        print("")


if __name__ == '__main__':
    ConsoleInteractive().start()