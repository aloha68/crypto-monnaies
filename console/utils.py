import logging
import datetime

logger = logging.getLogger(__name__)


def format_value(value, max_length=12):
    """Formate une valeur"""
    return "{:.10f}".format(value).rstrip('0').rstrip('.')


class ConsoleColor:
    """Accès facile à certaines couleurs pour la console"""
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


class ConsoleModule:
    commande = ''
    titre = ''

    def __init__(self, api):
        self.api = api

    def fill_arguments(self, parser):
        """Remplit les arguments saisissables en ligne de commande"""
        pass

    def parse_arguments(self, **kwargs):
        """Récupère les arguments passés à la ligne de commande"""
        pass

    def start(self):
        """Démarre le module"""
        raise NotImplementedError("{}.start() !".format(self.__class__.__name__))


class ConsoleStyleMixin:

    class Formats:
        BOLD = 1
        UNDERLINE = 2

    class Colors:
        PURPLE = '\033[95m'
        CYAN = '\033[96m'
        DARKCYAN = '\033[36m'
        BLUE = '\033[94m'
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        RED = '\033[91m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
        END = '\033[0m'
    
    def format_value(self, value, width=None, precision=None, sign='-', format=None, color=None):
        """Formate une valeur"""

        # format_spec: [[fill]align][sign][#][0][width][grouping_option][.precision][type]

        str_format = str(sign)
        if width:
            str_format += str(width)
        if precision is not None:
            str_format = str_format + '.' + str(precision)

        str_format = '{:' + str_format + 'f}'

        try:
            result = str_format.format(value)
        except ValueError as e:
            logger.error("Erreur de formatage:" + str_format)
            raise e

        if format == ConsoleStyleMixin.Formats.BOLD:
            result = self.bold(result)
        elif format == ConsoleStyleMixin.Formats.UNDERLINE:
            result = self.underline(result)

        if color:
            result = color + result + ConsoleColor.END

        return result

    def bold(self, text):
        """Met en gras le texte passé en paramètre"""
        return ConsoleColor.BOLD + str(text) + ConsoleColor.END

    def underline(self, text):
        """Met en italique le texte passé en paramètre"""
        return ConsoleColor.UNDERLINE + str(text) + ConsoleColor.END


class ConsoleChoice:
    def __init__(self, num, titre, callback=None, **kwargs):
        self.num = num
        self.titre = titre
        self.callback = callback
        self.args = kwargs
        self.result = None


class ConsoleChoiceSeparator:
    pass


class ConsoleChoiceLabel:
    def __init__(self, label):
        self.label = label


class ConsoleChoiceMenu:
    def __init__(self, titre, prompt, default_choice=None):

        if not prompt.endswith(' '):
            prompt += ' '

        self.titre = titre
        self.prompt = prompt
        self.default_choice = default_choice

        self.liste = []
        self.choix = []
        self.current_num = 0

    def _creer_choix(self, titre, num=None, callback=None, **kwargs):
        """Méthode interne pour créer un choix"""

        if not isinstance(num, int):
            self.current_num += 1
            num = self.current_num

        choix = ConsoleChoice(self.current_num, titre, callback=callback, **kwargs)
        return choix

    def _ajouter_choix(self, choix):
        """Méthode interne pour ajouter un choix"""
        if not isinstance(choix, ConsoleChoice):
            raise ValueError("On ne peut ajouter que des choix classiques")
        self.liste.append(choix)
        self.choix.append(choix)

    def ajouter_choix(self, titre, callback=None, **kwargs):
        """Ajoute un choix au menu"""
        choix = self._creer_choix(titre, callback=callback, **kwargs)
        self._ajouter_choix(choix)

    def ajouter_separateur(self):
        """Ajoute un séparateur au menu"""
        self.liste.append(ConsoleChoiceSeparator())

    def ajouter_label(self, label):
        """Ajoute un label au menu"""
        self.liste.append(ConsoleChoiceLabel(label))

    def faire_choix(self):
        """
        Exécute le menu jusqu'à ce qu'un choix correct soit effectué
        Si c'est le cas, on exécute alors le callback du choix si possible
        """
        if self.titre:
            print(self.titre)

        # Affichage du menu
        for opt in self.liste:
            if isinstance(opt, ConsoleChoice):
                print("  {}) {}".format(opt.num, opt.titre))
            elif isinstance(opt, ConsoleChoiceLabel):
                print(opt.label)
            elif isinstance(opt, ConsoleChoiceSeparator):
                print("")

        # Affichage de l'option par défaut si nécessaire
        if self.default_choice:
            print("  0) {}".format(self.default_choice))

        choix = None
        while True:

            user_input = input(self.prompt)
            if not user_input:
                break

            try:
                choix = int(user_input)
            except ValueError:
                continue

            if not choix:
                break

            for opt in self.choix:
                if opt.num == choix:
                    if callable(opt.callback):
                        opt.result = opt.callback(**opt.args)
                    return opt

        return choix


class ConsoleInputMixin:

    def creer_menu(self, titre, prompt="Votre choix : ", default_choice=None):
        menu = ConsoleChoiceMenu(titre, prompt, default_choice=default_choice)
        return menu

    def faire_choix(self, question, options):
        """Pose une question et attend une questions parmi les options possibles"""

        numbers = []
        for opt in options:

            if not isinstance(opt.num, int):
                raise ValueError("ConsoleInputMixin.faire_choix: Un choix n'a pas de numéro")
            if opt.num in numbers:
                raise ValueError("ConsoleInputMixin.faire_choix: Un choix a déjà ce numéro")

            print("  {}) {}".format(opt.num, opt.titre))
            numbers.append(opt.num)

        if 0 not in numbers:
            print("  0) Retour")

        if not question.endswith(' '):
            question = question + ' '

        choix = None
        while True:

            user_input = input(question)
            if not user_input:
                break

            try:
                choix = int(user_input)
            except ValueError:
                continue

            if not choix:
                break

            for opt in options:
                if opt.num == choix:
                    if callable(opt.callback):
                        opt.callback(**opt.args)
                    return choix

        return choix

    def saisir_booleen(self, question, default=None):

        result = None
        help_text = "[o/n]"
        if default == True:
            help_text = "[O/n]"
        elif default == False:
            help_text = "[o/N]"

        reponses_oui = ['o', 'oui', 'y', 'yes']
        reponses_non = ['n', 'no', 'non']

        if not question.endswith(' '):
            question += ' '
        question += help_text + ' '

        while True:

            saisie = input(question).lower()
            if not saisie and default is not None:
                return default

            if saisie in reponses_oui:
                return True
            elif saisie in reponses_non:
                return False

    def saisir_flottant(self, question, allow_blank=False):

        if not question.endswith(' '):
            question += ' '

        while True:
            try:
                user_input = input(question).replace(',', '.')
                if not user_input and allow_blank:
                    return None
                return float(user_input)
            except ValueError:
                pass

    def saisir_chaine(self, question, allow_blank=False):

        if not question.endswith(' '):
            question += ' '

        while True:
            user_input = input(question)
            if user_input or allow_blank:
                return user_input

    def saisir_date(self, question, default_now=False):

        if not question.endswith(' '):
            question += ' '

        # Formats de date acceptées
        date_formats = [
            "%d/%m/%Y %H:%M:%S",
            "%d/%m/%Y %H:%M",
            "%d/%m/%Y",
            "%d/%m/%y %H:%M:%S",
            "%d/%m/%y %H:%M",
            "%d/%m/%y",
            "%Y-%m-%d %H:%M:%S",
            "%Y-%m-%d",
        ]

        if default_now:
            question += "[{}] ".format(datetime.datetime.now().strftime(date_formats[0]))
        else:
            question += "(format : DD/MM/YY [HH:MM[:SS]]) "

        while True:
            user_input = input(question)

            # Si aucune valeur saisie et que la valeur par défaut est possible
            if not user_input and default_now:
                return datetime.datetime.now()

            elif not user_input:
                continue

            # On teste les différents formats de date
            for date_format in date_formats:
                try:
                    return datetime.datetime.strptime(user_input, date_format)
                except ValueError:
                    pass


if __name__ == '__main__':
    c = ConsoleStyleMixin()
    for t in [0, 1.3456, 1.34567890123456, 12345, 12345.789012345, 123456789012345]:
        val = c.format_value(
            t,
            width=16,
            # format=ConsoleStyleMixin.Formats.BOLD,
            color=ConsoleStyleMixin.Colors.CYAN
        )
        print("{:>16} --> len = {}".format('+' + val, len(val)))
        print("je vaux {} $$".format(val))
