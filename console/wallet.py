import logging

from orm.models import User, Currency, Operation
from settings import WALLET_USER
from .utils import (
    ConsoleModule, ConsoleInputMixin, ConsoleStyleMixin,
    ConsoleColor, format_value
)

logger = logging.getLogger(__name__)
feature_header = "\n(>'-')> <('_'<) ^('_')\\- \\m/(-_-)\\m/ <( '-')> \\_( .\")> <( ._.)-`\n"


class ConsoleWalletManager(ConsoleInputMixin, ConsoleStyleMixin, ConsoleModule):
    """
    Module permettant de gérer des portefeuilles et ainsi
    faciliter ses décisions lors de l'achat ou de la vente.
    Ce module permet également d'analyser ses précédents
    achats par le biais d'opérations à saisir.
    """
    commande = 'wallet'
    titre = "Vérifier son stock"

    # FORMAT     "      _       SOLDE_      UNITAIRE_     VALEUR_      ACHAT_       GAIN_      %"
    ROW_HEADER = " MONNAIE      SOLDE       UNITAIRE      VALEUR       ACHAT        GAIN       %"
    ROW_FORMAT = "   {monnaie} {solde:>12} {prix:>12} $ {valeur:>9} $ {achat:>9} $ {gain:>9} $ {pourcentage:>6}%"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self.username = WALLET_USER or ""
        self.list_only = False

    def fill_arguments(self, parser):
        """Remplit les arguments saisissables en ligne de commande"""
        parser.add_argument(
            '-l', '--list', action='store_true',
            help="Affiche un résumé des portefeuilles et quitte"
        )
        parser.add_argument(
            '-u', '--user', type=str,
            help="Nom d'utilisateur",
        )

    def parse_arguments(self, **kwargs):
        """Récupère les arguments passés à la ligne de commande"""
        self.username = kwargs.get('user') or self.username
        self.list_only = kwargs.get('list') or self.list_only

    def get_wallets(self):
        """Récupération des wallets de l'utilisateur"""
        return self.user.wallets.all()

    def generer_menu(self):
        """Création du menu"""

        # Récupération des portefeuilles de l'utilisateur
        wallets = self.get_wallets()

        menu = self.creer_menu("Bienvenue {} !".format(self.user.name), default_choice="Quitter")
        for wallet in wallets:
            titre = "{}, solde = {} {}".format(
                self.bold(wallet.currency.name.upper()),
                wallet.balance,
                wallet.currency.symbol.upper()
            )
            menu.ajouter_choix(titre, callback=self.gerer_portefeuille, wallet=wallet)

        if wallets:
            menu.ajouter_separateur()
            menu.ajouter_choix("Afficher un résumé", callback=self.resume_portefeuilles)
        menu.ajouter_choix("Ajouter un portefeuille", callback=self.ajouter_portefeuille)

        return menu

    def start(self):
        """Démarre le module"""

        # Saisie du nom d'utilisateur si non renseigné
        while not self.username:
            print("")
            name = input("Quel est votre nom ? ")
            if 0 < len(name) <= User.name.max_length:
                self.username = name

        # Récupération de l'utilisateur
        self.user, is_created = User.objects.get_or_create(name=self.username)

        if self.list_only:
            self.resume_portefeuilles()
            print("")
            return

        # Boucle infinie tant que l'utilisateur ne quitte pas
        while True:
            print(feature_header)
            menu = self.generer_menu()

            # Saisie du choix : l'appel au callback est compris dans la méthode
            try:
                if not menu.faire_choix():
                    break
            except KeyboardInterrupt:
                print("")
                print("Interruption par l'utilisateur...")
                break

        print("")
        print("À bientôt !")
        print("")

    def gerer_portefeuille(self, wallet):
        """Gère un portefeuille"""
        wallet_mgr = ConsoleWallet(self.api, wallet)
        wallet_mgr.start()

    def ajouter_monnaie(self):
        """Ajoute une monnaie"""

        print(feature_header)
        logger.debug("Ajouter une monnaie...")
        print("Ajouter une monnaie :")

        # Saisie du symbole
        while True:
            symbole = self.saisir_chaine("  Symbole :").lower()
            if len(symbole) == 3:
                break

        monnaie, is_created = Currency.objects.get_or_create(symbol=symbole)
        if not is_created:
            print("La monnaie existe déjà !")
            logger.debug("La monnaie {} existe déjà !".format(symbole))
            return monnaie

        # Saisie de la monnaie
        while True:
            nom_monnaie = self.saisir_chaine("  Monnaie :").upper()
            if nom_monnaie:
                break

        # Saisie du FIAT ou non
        while True:
            is_fiat = self.saisir_booleen("  Monnaie FIAT ?", default=False)
            if is_fiat is not None:
                break

        monnaie.name = nom_monnaie
        monnaie.is_fiat = is_fiat
        monnaie.save()

        print("La monnaie a bien été ajoutée !")
        logger.debug("Création de la monnaie {} ({}) !".format(symbole, nom_monnaie))
        return monnaie

    def choisir_monnaie(self, monnaies=None):
        """Choisit une monnaie parmi une liste définie ou à partir de toutes les monnaies non-fiat"""

        if not monnaies:
            monnaies = Currency.objects.filter(is_fiat=False)

        if not monnaies:
            print("Vous n'avez aucune monnaie disponible")
            return self.ajouter_monnaie()

        menu = self.creer_menu("Monnaies disponibles :", "Votre choix :", "Retour")
        for monnaie in monnaies:
            menu.ajouter_choix(monnaie.name, monnaie=monnaie)

        menu.ajouter_separateur()
        menu.ajouter_choix("Ajouter une monnaie", callback=self.ajouter_monnaie)

        while True:

            choix = menu.faire_choix()
            if not choix:
                break

            # Retourne le résultat du callback d'ajout de monnaie OU la monnaie existante sélectionnée
            return choix.result or choix.args['monnaie']

    def ajouter_portefeuille(self):
        """Ajoute un portefeuille"""

        print(feature_header)
        logger.debug("Ajouter un portefeuille...")
        print("Ajouter un portefeuille :")

        try:
            monnaie = self.choisir_monnaie()
        except KeyboardInterrupt:
            monnaie = None
            print("")

        if not monnaie:
            logger.debug("Aucune monnaie sélectionnée... Retour")
            print("Aucune monnaie sélectionnée... Retour !")
            return

        wallet, is_created = self.user.wallets.get_or_create(
            currency=monnaie,
            defaults={'balance': 0}
        )

        if not is_created:
            print("Le portefeuille existe déjà !")
        else:
            print("Le portefeuille a bien été ajouté !")
            logger.debug("Portefeuille ajouté !")

        return wallet

    def resume_portefeuilles(self):
        """Affiche un résumé de tous les wallets disponibles"""

        print("")
        print(self.underline("Résumé de vos portefeuilles"))
        print("")
        print(self.ROW_HEADER)

        total = 0
        wallets = self.get_wallets()
        for w in wallets:
            mgr = ConsoleWallet(self.api, w)
            print(self.ROW_FORMAT.format(
                monnaie=self.bold(w.currency.symbol.upper()),
                solde=self.format_value(w.balance),
                prix=self.format_value(mgr.valeur_unitaire, precision=2),
                valeur=self.format_value(mgr.valeur, width=6, precision=2),
                achat=self.format_value(mgr.valeur_achat, width=6, precision=2),
                gain=self.format_value(mgr.gain, width=6, precision=2),
                pourcentage=self.format_value(mgr.pourcentage_gain, precision=0, sign='+')
            ))

            total += mgr.valeur

        str_total = self.bold("{} $".format(self.format_value(total, precision=2)))
        print("")
        print("  Vous avec {} au total !".format(str_total))


class ConsoleWallet(ConsoleStyleMixin, ConsoleInputMixin):
    """
    Classe de gestion d'un portefeuille unique
    """

    def __init__(self, api, wallet):
        self.api = api
        self.wallet = wallet
        self._usd_tick = None
        self._buy_price = 0
        self._real_buy_price = 0
        self._is_operations_parsed = False
        self.reload_tick()

    @property
    def usd_tick(self):
        """Tick représentant la monnaie du wallet vers le dollar"""
        if self._usd_tick:
            return self._usd_tick

        self.reload_tick()
        return self._usd_tick

    @property
    def valeur(self):
        """Valeur du portefeuille en dollar"""
        return self.wallet.balance * self.valeur_unitaire

    @property
    def valeur_unitaire(self):
        """Valeur unitaire de la monnaie du portefeuille"""
        return self.usd_tick.last_price

    @property
    def valeur_achat(self):
        """Valeur d'achat en $ des jetons"""
        if not self._is_operations_parsed:
            self.reload_operations()
        return self._buy_price

    @property
    def valeur_achat_reelle(self):
        """Valeur d'achat réelle des jetons"""
        if not self._is_operations_parsed:
            self.reload_operations()
        return self._real_buy_price

    @property
    def gain(self):
        return self.valeur - self.valeur_achat

    @property
    def pourcentage_gain(self):
        if not self.valeur_achat:
            return 0
        return self.gain / self.valeur_achat * 100

    def reload_tick(self):
        """Recharge un nouveau tick pour le portefeuille en cours"""

        if self._usd_tick:
            logger.debug("Rechargement du tick")
        else:
            logger.debug("Chargement du tick")

        pair = self.wallet.currency.symbol.lower() + 'usd'
        self._usd_tick = self.api.get_tick(pair)

    def reload_operations(self):
        """Recharge les opérations du portefeuille en cours"""

        self._buy_price = 0
        self._real_buy_price = 0

        operations = self.wallet.operations.all()
        for op in operations:
            if op.type == Operation.TYPE_IN:
                self._buy_price += op.usd_value
                self._real_buy_price += op.real_value

        self._is_operations_parsed = True

    def ajouter_operation(self):
        """Ajout d'une opération"""

        print(feature_header)
        self._debug("Ajout d'une opération")
        print("Ajouter une opération :")

        valeur = 0
        type_transaction = Operation.TYPE_IN

        # On cache le KeyboardInterrupt pendant la saisie
        try:
            date = self.saisir_date("Date")

            while True:

                user_input = self.saisir_chaine("Nombre de jetons (préfixé par '+' ou '-')")
                if not user_input[0] in '+-':
                    continue

                if user_input[0] == '+':
                    type_transaction = Operation.TYPE_IN
                elif user_input[0] == '-':
                    type_transaction = Operation.TYPE_OUT

                user_input = user_input[1:]

                try:
                    valeur = float(user_input.replace(',', '.'))
                    break
                except ValueError:
                    pass

            valeur_usd = self.saisir_flottant("Valeur en dollars :")
            valeur_reelle = self.saisir_flottant("Valeur réelle en euros (sans taxes) :")

            self.wallet.add_operation(
                date=date, type=type_transaction,
                value=valeur, usd_value=valeur_usd, real_value=valeur_reelle)

            self._is_operations_parsed = False

        except KeyboardInterrupt:
            self._debug("Ajout d'une opération : abandonné")
            print("\nSaisie interrompue !")
            return

    def lister_operations(self):
        """Affiche la liste des opéations pour un portefeuille"""

        print(feature_header)
        self._debug("Liste des opérations")

        operations = self.wallet.operations.all()
        if not operations:
            print("Vous n'avez aucune opération pour le moment")
            return

        #        " 01/01/17_+0.1234567890_0.1234567890_0.1234567890_0.1234567890"
        header = "     DATE         SOLDE     PRIX ($) UNITAIRE ($)     RÉEL (€)"
        format = " {:%d/%m/%y} {} {} {} {}"

        print("Liste des opérations :")
        print(header)

        # Affichage des opérations
        for op in operations:

            sign = '+'
            if op.type == Operation.TYPE_OUT:
                sign = '-'

            prix_unitaire = op.usd_value / op.value
            print(format.format(
                op.date,
                self.format_value(op.value, width=13, sign=sign),
                self.format_value(op.usd_value, width=12, precision=2),
                self.format_value(prix_unitaire, width=12, precision=2),
                self.format_value(op.real_value, width=12, precision=2)
            ))

        print("")
        input("Entrée pour continuer ")

    def afficher(self):
        """Affiche les détails du portefeuille"""

        operations = self.wallet.operations.all()

        print(self.underline("Portefeuille {}".format(self.wallet.currency.name.upper())))
        print("  Solde           {} {}".format(self.bold(self.wallet.balance), self.wallet.currency.symbol.upper()))
        print("  Valeur actuelle {} $".format(self.format_value(self.valeur, precision=2)))

        print("  Valeur  {} $".format(self.format_value(self.valeur_achat, precision=2)))
        print("  Réelle  {} €".format(self.format_value(self.valeur_achat_reelle, precision=2)))

    def definir_nouveau_solde(self):
        """Définit un nouveau solde pour un portefeuille"""

        print(feature_header)
        self._debug("Définir un nouveau solde")

        print("Vous avez demandé à définir un nouveau solde à la main.")
        print("Pour une analyse future de votre portefeuille, il vaut mieux saisir des opérations manuellement.")
        print("Ctrl-C ou ne rien saisir pour annuler.")
        print("")

        try:
            value = self.saisir_flottant("Nouveau solde :", allow_blank=True)
        except KeyboardInterrupt:
            value = None
            print("")

        if value is None:
            self._debug("Définir un nouveau solde : abandonné")
            return

        print("")
        print("Vous avez défini un nouveau solde de {} {}".format(value, self.wallet.currency.symbol.upper()))
        print("Afin de pouvoir analyser le portefeuille, il est conseillé de passer par une opération.")

        if self.saisir_booleen("Voulez-vous tout de même procéder au changement ?", default=False):
            self._debug("Nouveau solde sans opérations : de {} à {}".format(self.wallet.balance, value))
            self.wallet.balance = value
            self.wallet.save()

    def start(self):
        """Débute la gestion du portefeuille portefeuille"""

        self._info("Ouverture du portefeuille")

        menu = self.creer_menu("Options", default_choice="Retour")
        menu.ajouter_choix("Ajouter une operation", self.ajouter_operation)
        menu.ajouter_choix("Lister les opérations", self.lister_operations)
        menu.ajouter_choix("Définir un nouveau solde", self.definir_nouveau_solde)

        while True:
            print(feature_header)
            self.afficher()
            print("")

            choix = menu.faire_choix()
            if not choix:
                break

        self._info("Fermeture du portefeuille")

    def _get_log_message(self, message):
        return "Wallet {}/{}: {}".format(self.wallet.user.name, self.wallet.currency.symbol, message)

    def _info(self, message):
        """Ajoute un message d'info"""
        logger.info(self._get_log_message(message))

    def _debug(self, message):
        """Ajoute un message d'erreur"""
        logger.debug(self._get_log_message(message))
