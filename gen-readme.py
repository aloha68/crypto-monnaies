#!/usr/bin/env python

import argparse
import manage
from console import ConsoleModule, ConsoleInteractive

console = ConsoleInteractive()


def print_installation():

    print("## Installation")
    print("""
Avec un environnement python + virtualenv + pip et un interpréteur de commande **bash**

    git clone git@gitlab.com:aloha68/crypto-monnaies.git
    cd crypto-monnaies
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt""")


def print_utilisation():

    parser = console.get_parser()
    usage = parser.format_usage().split(maxsplit=1)[1].strip()

    print("""## Utilisation
    
Format de la ligne de commande

    {}    

Il est possible de lire l'aide du logiciel avec l'option **-h**, ou encore d'activer le mode verbeux avec l'option **-v**      
Le mode verbeux peut se stacker, jusqu'à deux occurences au maximum pour le moment.

**Attention !**  
Chaque sous-commande possède ses propres arguments.  
Pour les lister, il est possible d'afficher l'aide d'une commande avec l'option **-h**, mais situé derrière le nom de la commande.

Par exemple :

    {} subcommand -h""".format(usage, parser.prog))


def print_module(module_class):
    m = module_class(api=None)

    print("### {}".format(m.titre))
    print("")
    print("Commande associée: **{}**  ".format(m.commande))
    print("Classe associée: *{}.{}*".format(module_class.__module__, module_class.__name__))

    doc = module_class.__doc__
    if doc:
        print("")
        lines = []
        for line in doc.split('\n'):
            line = line.strip()
            if line:
                lines.append(line)
        print(" ".join(lines))

    parser = console.get_parser()
    module_parser = argparse.ArgumentParser()
    module_parser.prog = "{} {}".format(parser.prog, m.commande)
    m.fill_arguments(module_parser)
    help = module_parser.format_help().rstrip(' ')
    if help:
        print("")
        for line in help.split('\n'):
            print('    ' + line)


def print_modules():

    print("## Liste des modules")
    modules = ConsoleModule.__subclasses__()
    for m in modules:
        print("")
        print_module(m)

if __name__ == '__main__':

    parser = console.get_parser()

    print("")
    print("# {}".format(parser.prog))
    print("")
    print(parser.description)
    print("")
    print_installation()
    print("")
    print_utilisation()
    print("")
    print_modules()
