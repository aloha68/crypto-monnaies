
# crypto.py

Parseur de crypto-monnaie homemade :-)

## Installation

Avec un environnement python + virtualenv + pip et un interpréteur de commande **bash**

    git clone git@gitlab.com:aloha68/crypto-monnaies.git
    cd crypto-monnaies
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt

## Utilisation
    
Format de la ligne de commande

    crypto.py [-h] [-v] {buy,convert,wallet,watch} ...    

Il est possible de lire l'aide du logiciel avec l'option **-h**, ou encore d'activer le mode verbeux avec l'option **-v**      
Le mode verbeux peut se stacker, jusqu'à deux occurences au maximum pour le moment.

**Attention !**  
Chaque sous-commande possède ses propres arguments.  
Pour les lister, il est possible d'afficher l'aide d'une commande avec l'option **-h**, mais situé derrière le nom de la commande.

Par exemple :

    crypto.py subcommand -h

## Liste des modules

### Simuler un achat

Commande associée: **buy**  
Classe associée: *console.buyer.ConsoleBuyer*

Module permettant de simuler un achat, c'est-à-dire d'estimer la valeur d'une monnaie à un moment précis.

    usage: crypto.py buy [-h] [--usd USD] [nombre] monnaie
    
    positional arguments:
      nombre      Nombre de jetons à acheter (par défaut : 1)
      monnaie     Symbole représentant la monnaie (en trois lettres)
    
    optional arguments:
      -h, --help  show this help message and exit
      --usd USD   Nombre de dollars à investir
    

### Convertir une monnaie dans une autre

Commande associée: **convert**  
Classe associée: *console.converter.ConsoleConverter*

Module permettant de convertir la valeur d'une monnaie dans une autre.

    usage: crypto.py convert [-h] -in IN [-out OUT] valeur
    
    positional arguments:
      valeur      Valeur de la monnaie à convertir
    
    optional arguments:
      -h, --help  show this help message and exit
      -in IN      Monnaie entrante
      -out OUT    Monnaie sortante
    

### Vérifier son stock

Commande associée: **wallet**  
Classe associée: *console.wallet.ConsoleWalletManager*

Module permettant de gérer des portefeuilles et ainsi faciliter ses décisions lors de l'achat ou de la vente. Ce module permet également d'analyser ses précédents achats par le biais d'opérations à saisir.

    usage: crypto.py wallet [-h] [-l] [-u USER]
    
    optional arguments:
      -h, --help            show this help message and exit
      -l, --list            Affiche un résumé des portefeuilles et quitte
      -u USER, --user USER  Nom d'utilisateur
    

### Observer le marché

Commande associée: **watch**  
Classe associée: *console.watcher.ConsoleWatcher*

Module permettant de suivre des indices périodiquement. À chaque interrogation de l'API bitfinex, une nouvelle ligne est ajoutée à la console pour indiquer le taux en cours pour la liste des paires à observer.

    usage: crypto.py watch [-h] pair [pair ...]
    
    positional arguments:
      pair        Pair à suivre (ex: btcusd, iotusd, iotbtc)
    
    optional arguments:
      -h, --help  show this help message and exit
    
