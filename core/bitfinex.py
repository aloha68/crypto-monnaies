import logging

from .factories import AbstractAPI
from .models import Tick

logger = logging.getLogger(__name__)


class MyBitfinexAPI(AbstractAPI):

    def __init__(self):
        super().__init__()
        self.base_url = "https://api.bitfinex.com/v1/"

    def _get_pairs(self):
        return self._get(url='symbols')

    def _get_symbols_from_pair(self, pair):
        pair = pair.lower()
        symbol_from = pair[:3]
        symbol_to = pair[3:]
        return symbol_from, symbol_to

    def _get_tick(self, pair):
        """Retourne un tick de valeur entre deux devises"""

        currency_from, currency_to = self._get_symbols_from_pair(pair)

        data = self._get(url='pubticker/{}'.format(pair))
        tick = Tick(currency_from, currency_to, data)
        return tick