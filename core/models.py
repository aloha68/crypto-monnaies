import datetime


class Tick:
    """Représente un tick à un moment T de la valeur d'une monnaie dans une autre"""
    def __init__(self, currency_from, currency_to, values=None):

        self.currency_from = currency_from
        self.currency_to = currency_to
        self.symbol = currency_from + currency_to

        values = values or {}
        self.mid = float(values.get('mid', 0))
        self.bid = float(values.get('bid', 0))
        self.ask = float(values.get('ask', 0))
        self.last_price = float(values.get('last_price', 0))
        self.low = float(values.get('low', 0))
        self.high = float(values.get('high', 0))
        self.volume = float(values.get('volume', 0))
        self.date = datetime.datetime.utcfromtimestamp(float(values.get('timestamp', 0)))

    def __str__(self):
        return "{:%H:%M:%S} {} to {} : {:f}".format(
            self.date,
            self.currency_from.upper(),
            self.currency_to.upper(),
            self.last_price
        )