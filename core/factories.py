import logging
from .api import AbstractAPI
from .exceptions import APIException

logger = logging.getLogger(__name__)


class APIFactory:

    @staticmethod
    def get_api(service='bitfinex'):
        """
        Retourne l'API associé au service dont le nom est passé en paramètre
        :param service: Seul bitfinex est reconnu !
        :return: AbstractAPI
        """

        api = None
        service = service.lower()
        logger.debug("Recherche de l'API: {}".format(service))

        # Création de l'api
        if service == 'bitfinex':
            from .bitfinex import MyBitfinexAPI
            api = MyBitfinexAPI()

        # Si le service n'existe pas
        if not api:
            raise APIException("API non trouvée (service: {})".format(service))

        # Si l'api n'implémente pas la bonne classe
        if not isinstance(api, AbstractAPI):
            raise APIException(
                "La classe {} n'implémente pas la classe AbstractAPI".format(api.__class__.__name__))

        return api
