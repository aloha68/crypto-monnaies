import requests
import logging
import threading

logger = logging.getLogger(__name__)


class TickerWatcher(threading.Thread):
    def __init__(self, api, symbol, callback, max_errors=0, waiting_time=30):
        super().__init__()
        self._api = api
        self._symbol = symbol
        self._callback = callback
        self._max_errors = max_errors
        self._waiting_time = waiting_time
        self._stop_event = threading.Event()

        self.currency = symbol[:3].upper()
        self.destination = symbol[3:].upper()

        self.first_ticker = None
        self.current_ticker = None
        self.last_ticker = None

    @property
    def symbol(self):
        return self._symbol

    def start(self):
        """Start watching inside thread"""
        if not callable(self._callback):
            raise Exception("Callback of watcher if not callable")
        super().start()

    def stop(self):
        """Stop watching"""
        self._stop_event.set()

    def run(self):
        """Run thread to watching pair value"""

        while not self._stop_event.is_set():

            # Setting last ticker if possible
            if self.current_ticker:
                self.last_ticker = self.current_ticker

            # Getting ticker from api
            while True:
                try:
                    self.current_ticker = self._api.get_tick(self._symbol)
                    break
                except requests.exceptions.HTTPError as e:
                    logger.warning("TickerWatcher: HTTPError: {}".format(e.strerror))
                    self._max_errors -= 1
                    if self._max_errors <= 0:
                        raise e

            # Setting first ticker
            if not self.first_ticker:
                self.first_ticker = self.current_ticker

            # Calling callback
            self._callback(self)

            logger.debug("{} watcher going to sleep for {} seconds...".format(
                self._symbol, self._waiting_time))

            self._stop_event.wait(self._waiting_time)

