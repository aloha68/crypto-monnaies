import json
import requests
import requests.exceptions
import logging

from .exceptions import APIPairNotFound, APIConnectionError

logger = logging.getLogger(__name__)


class AbstractAPI:
    """
    Classe représentant une API de base.
    Elle définit des méthodes spécifiques à implémenter.
    """

    base_url = ""

    def __init__(self):
        self._pairs = []

    def _get_pairs(self):
        """Retourne la liste des paires supportées au format list"""
        raise NotImplementedError("{}._get_pairs".format(self.__class__.__name__))

    def _get_symbols_from_pair(self, pair):
        """Retourne un tuple contenant le symbole de la
        première et de la deuxième monnaie du pair.
        Exemple: pair = 'btcusd', result = ('btc', 'usd')"""
        raise NotImplementedError("{}._get_symbols_from_pair".format(self.__class__.__name__))

    def _get_tick(self, pair):
        """Retourne un tick de valeur entre deux devises"""
        raise NotImplementedError("{}._get_pairs".format(self.__class__.__name__))

    def _get(self, url, method='GET'):
        """
        Interroge l'url en paramètre et retourne
        le résultat au format python (liste ou dict)
        :param url:
        :param method:
        :return:
        """

        if not url.startswith(self.base_url):
            url = self.base_url + url
        logger.debug("API send request to: {}".format(url))

        try:
            response = requests.request(method, url)
        except requests.exceptions.HTTPError as e:
            raise APIConnectionError(str(e))

        if not response.ok:
            response.raise_for_status()

        data = json.loads(response.text)
        return data

    @property
    def pairs(self):
        """Retourne la liste des pairs"""
        if self._pairs:
            return self._pairs

        pairs = self._get_pairs()
        if not isinstance(pairs, list):
            raise TypeError("La liste des paires doit être une liste !")

        self._pairs = pairs
        return self._pairs

    def get_tick(self, pair):
        """Retourne un tick de valeur entre deux devises"""

        if not pair in self.pairs:
            raise APIPairNotFound(pair)

        return self._get_tick(pair)
