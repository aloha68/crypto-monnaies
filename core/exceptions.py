
class APIException(Exception):
    def __init__(self, message):
        self.message = str(message)
        super().__init__(message)

    def __str__(self):
        return self.message


class APIConnectionError(APIException):
    def __init__(self, str_error):
        super().__init__("Erreur HTTP: {}".format(str_error))


class APICurrencyNotFound(APIException):
    def __init__(self, symbole):
        super().__init__("La monnaie '{}' n'est pas référencée par l'API".format(symbole))


class APIPairNotFound(APIException):
    def __init__(self, paire):
        super().__init__("La pair '{}' n'est pas référencée par l'API".format(paire))