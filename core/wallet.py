
class Wallet:

    def __init__(self, monnaie, solde):
        self._monnaie = monnaie
        self._solde = solde

    def __str__(self):
        return "Wallet {}: {}".format(self._monnaie, self._solde)
