
import unittest
from .utils import create_seed


class TestIota(unittest.TestCase):

    def test_create_seed(self):
        seed = create_seed()
        self.assertEqual(len(seed), 81)
