
import secrets
import string


def create_seed():
    return ''.join(secrets.choice(string.ascii_uppercase + '9') for _ in range(81))


if __name__ == '__main__':
    seed = create_seed()
    print(seed)