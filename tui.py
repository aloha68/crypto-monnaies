
import logging
import sys
import threading
from collections import defaultdict

from asciimatics.event import KeyboardEvent
from asciimatics.exceptions import ResizeScreenError, StopApplication
from asciimatics.scene import Scene
from asciimatics.screen import Screen
from asciimatics.widgets import Frame, Layout, Label, Text

from core.bitfinexe import MyBitfinexApi

# logging.basicConfig(level=logging.DEBUG)
logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

API = MyBitfinexApi()


class PairWatcher(threading.Thread):
    
    def __init__(self, api, pair, callback=None, waiting_time=30):
        super().__init__()
        self._api = api
        self.pair = pair
        self.callback = callback
        self._waiting_time = waiting_time
        self._stop_event = threading.Event()
        
        self.currency = pair[:3].upper()
        self.destination = pair[3:].upper()
        
        self.history = []
        self.first_ticker = None
        self.current_ticker = None
        self.last_ticker = None
        
    def start(self):
        if not callable(self.callback):
            raise Exception("Callback of watcher if not callable")
            
        super().start()
        
    def stop(self):
        self._stop_event.set()
        
    def run(self):
        """Run thread to watching pair value"""
        
        while not self._stop_event.is_set():
            
            # Setting first ticker
            if not self.first_ticker:
                self.first_ticker = self.current_ticker
                
            # Setting last ticker if possible
            if self.current_ticker:
                self.last_ticker = self.current_ticker
                
            # Getting ticker from api
            self.current_ticker = self._api.ticker(self.pair)
            
            # Calling callback
            self.callback(self)
            
            logger.debug("{} watcher going to sleep for {} seconds...".format(self.pair, self._waiting_time))
            self._stop_event.wait(self._waiting_time)


class PairWatcherFrame(Frame):
    
    def __init__(self, watcher, screen, height, width, *args, **kwargs):
        
        if not watcher.callback:
            watcher.callback = self.update_watcher_values
            
        self.watcher = watcher
        self.nb_updates = 0
        self.need_update = False
        self.title = "{} - {}".format(watcher.currency, watcher.destination)
        
        super().__init__(screen, height, width, title=self.title, has_border=True, *args, **kwargs)
        
        self.build_layouts()
        self.build_palette()
        
    def build_layouts(self):
        """Build all layouts"""
        
        layout = Layout(columns=[1, 2, 1, 2, 2, 2])
        self.add_layout(layout)
        
        self.lbl_price = self.create_text()
        self.lbl_mid = self.create_text()
        self.lbl_bid = self.create_text()
        self.lbl_ask = self.create_text()
        self.lbl_low = self.create_text()
        self.lbl_high = self.create_text()
        self.lbl_volume = self.create_text(custom_colour=None)
        self.lbl_date = self.create_text(custom_colour=None)
        
        layout.add_widget(Label("PRIX"), column=0)
        layout.add_widget(self.lbl_price, column=1)
        layout.add_widget(Label("MOY"), column=0)
        layout.add_widget(self.lbl_mid, column=1)
        layout.add_widget(Label("MIN"), column=2)
        layout.add_widget(self.lbl_bid, column=3)
        layout.add_widget(Label("MAX"), column=2)
        layout.add_widget(self.lbl_ask, column=3)
        layout.add_widget(Label("24H MIN"), column=4)
        layout.add_widget(self.lbl_low, column=5)
        layout.add_widget(Label("24H MAX"), column=4)
        layout.add_widget(self.lbl_high, column=5)
        layout.add_widget(Label("24H VOLUME"), column=4)
        layout.add_widget(self.lbl_volume, column=5)
        layout.add_widget(self.lbl_date, column=5)
        
        self.fix()
        
    def build_palette(self):
        """Build palette"""
        self.palette = defaultdict(
            lambda: (Screen.COLOUR_WHITE, Screen.A_NORMAL, Screen.COLOUR_BLACK))
        self.palette["title"] = (Screen.COLOUR_GREEN, Screen.A_BOLD, Screen.COLOUR_BLACK)
        self.palette["label"] = (Screen.COLOUR_WHITE, Screen.A_NORMAL, Screen.COLOUR_BLACK)
        self.palette["label_value"] = (Screen.COLOUR_WHITE, Screen.A_BOLD, Screen.COLOUR_BLACK)
        self.palette["label_up"] = (Screen.COLOUR_GREEN, Screen.A_BOLD, Screen.COLOUR_BLACK)
        self.palette["label_down"] = (Screen.COLOUR_RED, Screen.A_BOLD, Screen.COLOUR_BLACK)
        
    def create_text(is_disabled=True, custom_colour="label_value"):
        """Create a text containing values"""
        text = Text()
        text.disabled = is_disabled
        if custom_colour:
            text.custom_colour = custom_colour
        return text

    def update_watcher_value(self, gui_label, ticker, last_ticker, attr):
        
        value = getattr(ticker, attr)
        gui_label.value = str(value)
        
        if not last_ticker:
            gui_label.custom_colour = "label_value"
            return
        
        last_value = getattr(last_ticker, attr)
        if value > last_value:
            gui_label.custom_colour = "label_up"
        elif value < last_value:
            gui_label.custom_colour = "label_down"
        else:
            gui_label.custom_colour = "label_value"

    def update_watcher_values(self, watcher):
        """Update values from current ticker and last ticker"""
        
        ticker = watcher.current_ticker
        last_ticker = watcher.last_ticker
        
        self.update_watcher_value(self.lbl_price, ticker, last_ticker, 'last_price')
        self.update_watcher_value(self.lbl_mid, ticker, last_ticker, 'mid')
        self.update_watcher_value(self.lbl_bid, ticker, last_ticker, 'bid')
        self.update_watcher_value(self.lbl_ask, ticker, last_ticker, 'ask')
        self.update_watcher_value(self.lbl_low, ticker, last_ticker, 'low')
        self.update_watcher_value(self.lbl_high, ticker, last_ticker, 'high')
        self.lbl_volume.value = str(ticker.volume)
        self.lbl_date.value = ticker.date.strftime("%H:%M:%S")
        
    def watcher_callback(self, *args):
        logger.debug("Need update")
        self.need_update = True
        
    def _update(self, frame_no):
        """Refresh view if needed"""
        
        if self.need_update:
            logger.debug("Doing update")
            self.update_watcher_values(self.watcher.current_ticker, self.watcher.last_ticker)
            self.need_update = False
            
        super()._update(frame_no)

    @property
    def frame_update_count(self):
        return 20

class AppScene(Scene):
    
    def __init__(self, screen):
        
        self._api = API
        
        self.screen = screen
        self.current_y = 1
        
        self.currencies = ['iotusd', 'btcusd', 'iotbtc']
        self.watchers = []
        
        effects = []
        for pair in self.currencies:
            effects.append(self.get_frame(pair))
        
        super().__init__(effects, duration=-1)
        
        self.start_watchers()
        
    def get_frame(self, currency):
        
        height = round((self.screen.height) / len(self.currencies)) - 2
        watcher = self.create_watcher(currency)
        
        frame = PairWatcherFrame(
            watcher,
            screen=self.screen, 
            height=height, 
            width=self.screen.width-2,
            x=1,
            y=self.current_y)
            
        self.current_y += height + 1
        return frame
        
    def create_watcher(self, pair):
        """Create a watcher with a pair"""
        
        # Check if key exists in API
        if not pair in self._api.pairs:
            raise ValueError("Pair {} not found".format(pair))
            
        # Check if key already exists in app
        names = [w.pair for w in self.watchers]
        if pair in names:
            raise ValueError("Pair {} already exists".format(pair))
            
        watcher = PairWatcher(self._api, pair, waiting_time=30)
        self.watchers.append(watcher)
        
        logger.debug("add_watcher: {}".format(pair))
        return watcher
        
    def start_watchers(self):
        """Start all watchers"""
        for watcher in self.watchers:
            watcher.start()
        
    def kill_watchers(self):
        """Kill all watchers"""
        for watcher in self.watchers:
            if watcher.is_alive():
                watcher.stop()
                watcher.join()
        
    def process_event(self, event):
        """Handle events"""
        if isinstance(event, KeyboardEvent):
            # Stop event
            if event.key_code in [ord('q'), ord('Q'), Screen.ctrl('c')]:
                self.kill_watchers()
                raise StopApplication("User quit")
        
        return super().process_event(event)


def run(screen):
    screen.play([AppScene(screen)], stop_on_resize=True)

if __name__ == '__main__':
    
    while True:
        try:
            Screen.wrapper(run)
            sys.exit(0)
        except ResizeScreenError:
            pass
