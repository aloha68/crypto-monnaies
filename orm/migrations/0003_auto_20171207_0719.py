# Generated by Django 2.0 on 2017-12-07 07:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0002_currency_transaction'),
    ]

    operations = [
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('balance', models.FloatField(verbose_name='Solde')),
            ],
        ),
        migrations.AddField(
            model_name='currency',
            name='is_fiat',
            field=models.BooleanField(default=False, verbose_name='Monnaie FIAT'),
        ),
        migrations.AlterField(
            model_name='currency',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Monnaie'),
        ),
        migrations.AlterField(
            model_name='currency',
            name='symbol',
            field=models.CharField(max_length=3, unique=True, verbose_name='Symbole'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='currency_from',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='currency_from', to='orm.Currency', verbose_name='Devise vendue'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='currency_to',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='currency_to', to='orm.Currency', verbose_name='Devise achetée'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orm.User', verbose_name='Utilisateur'),
        ),
        migrations.AddField(
            model_name='wallet',
            name='currency',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orm.Currency', verbose_name='Devise'),
        ),
        migrations.AddField(
            model_name='wallet',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orm.User', verbose_name='Utilisateur'),
        ),
    ]
