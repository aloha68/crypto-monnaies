import logging

from django.db import models, transaction

logger = logging.getLogger(__name__)


class User(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Currency(models.Model):
    name = models.CharField("Monnaie", max_length=50)
    symbol = models.CharField("Symbole", max_length=3, unique=True)
    is_fiat = models.BooleanField("Monnaie FIAT", default=False)

    class Meta:
        ordering = ['symbol']

    def save(self, *args, **kwargs):
        self.symbol = self.symbol.upper()
        self.name = self.name.capitalize()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Wallet(models.Model):
    user = models.ForeignKey(
        to=User,
        related_name='wallets',
        verbose_name="Utilisateur",
        on_delete=models.CASCADE
    )
    currency = models.ForeignKey(
        to=Currency,
        verbose_name="Devise",
        on_delete=models.CASCADE,
        limit_choices_to={'is_fiat': False}
    )
    balance = models.FloatField("Solde")

    class Meta:
        ordering = ['currency__name']

    def add_operation(self, **kwargs):
        """Ajoute une opération"""

        logger.debug("Ajout d'une opération: {}".format(kwargs))

        with transaction.atomic():
            op = self.operations.create(**kwargs)

            if op.type == Operation.TYPE_IN:
                self.balance += op.value
            elif op.type == Operation.TYPE_OUT:
                self.balance -= op.value
            else:
                raise ValueError("Le type de l'opération n'est pas correct")

            self.save()

    def __str__(self):
        return "Portefeuille de {}: {} {}".format(
            self.user.name,
            self.balance,
            self.currency.symbol
        )


class Operation(models.Model):
    TYPE_IN = "IN"
    TYPE_OUT = "OUT"
    TYPE_CHOICES = (
        (TYPE_IN, "Entrante"),
        (TYPE_OUT, "Sortante")
    )

    date = models.DateTimeField("Date")
    wallet = models.ForeignKey(
        to=Wallet,
        related_name='operations',
        verbose_name="Portefeuille",
        on_delete=models.CASCADE
    )
    type = models.CharField(max_length=3, choices=TYPE_CHOICES)
    value = models.FloatField("Montant")
    usd_value = models.FloatField("Montant en $")
    real_value = models.FloatField("Montant réel", default=0)

    class Meta:
        ordering = ['date']
